package com.example.tallerindividualandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class JugarActivity extends AppCompatActivity implements View.OnClickListener {
    TextView mostrar_i, mostrar_res;
    EditText numero;
    Button adivinar, nuevo;
    int vidas, numeAleatorio, numeroEdt, vidasAgotadas = 1;
    String num_int;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jugar);

        mostrar_i = findViewById(R.id.tvwIntentos);
        mostrar_res = findViewById(R.id.tvwResultados);
        numero = findViewById(R.id.edtNumero);
        adivinar = findViewById(R.id.btnAdivinar);
        nuevo = findViewById(R.id.btnNuevojuego);

        Intent i = getIntent();
        num_int = i.getStringExtra("intento");
        vidas = Integer.parseInt(num_int);
        mostrar_i.setText("Intentos: "+ (1));

        adivinar.setOnClickListener(this);
        nuevo.setOnClickListener(this);
        nuevo.setEnabled(false);

        numeAleatorio = (int)(Math.random()*(101 - 0 + 1) + 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAdivinar:
                String vacio = numero.getText().toString();

                if(vacio.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Ingresa un número", Toast.LENGTH_SHORT).show();
                }else{
                    if(vidas > vidasAgotadas){
                        numeroEdt = Integer.parseInt(numero.getText().toString());
                        if(numeAleatorio > numeroEdt){
                            mostrar_res.setText("El número es MAYOR que " + numeroEdt);
                        }else if(numeAleatorio < numeroEdt){
                            mostrar_res.setText("El número es MENOR que " + numeroEdt);
                        }else if(numeroEdt == numeAleatorio){
                            mostrar_res.setText("¡Haz ganado! El número es " + numeAleatorio);
                            adivinar.setEnabled(false);
                            nuevo.setEnabled(true);
                        }
                        mostrar_i.setText("Intentos: "+ (vidasAgotadas+1));
                        vidasAgotadas++;
                    }else{
                        mostrar_res.setText("¡Perdiste! El número era " + numeAleatorio);
                        adivinar.setEnabled(false);
                        nuevo.setEnabled(true);
                    }
                }

                break;
            case R.id.btnNuevojuego:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                break;
        }
    }

}