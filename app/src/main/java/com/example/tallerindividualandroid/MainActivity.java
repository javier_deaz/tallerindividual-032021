package com.example.tallerindividualandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Spinner intentos;
    Button jugar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        intentos = findViewById(R.id.spnIntentos);
        jugar = findViewById(R.id.btnJugar);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.C_intentos, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        intentos.setAdapter(adapter);
        jugar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()){
            case R.id.btnJugar:
                String opc = intentos.getSelectedItem().toString();
                if(opc.equals("5")){
                    i = new Intent(getApplicationContext(), JugarActivity.class);
                    i.putExtra("intento", opc);
                    startActivity(i);
                }else {
                    if(opc.equals("7")){
                        i = new Intent(getApplicationContext(), JugarActivity.class);
                        i.putExtra("intento", opc);
                        startActivity(i);
                    }else{
                        if(opc.equals("10")) {
                            i = new Intent(getApplicationContext(), JugarActivity.class);
                            i.putExtra("intento", opc);
                            startActivity(i);
                        }else{
                            if(opc.equals("")){
                                Toast.makeText(getApplicationContext(), "Elige el número de intentos", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
                break;
        }
    }

}